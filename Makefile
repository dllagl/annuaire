
all:
	make -C src/
	make -C tests/

check:
	make -C tests/

app:
	make -C src/

clean:
	make -C src/ clean
	make -C tests/ clean

distclean: 
	make -C src/ distclean
	make -C tests/ distclean