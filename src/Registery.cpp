#include "Registery.hpp"


std::shared_ptr<Contact> Registery::CreateContact() const {
    std::string name, lastName, number;

    // ask for name
    std::cout << "Please enter new contact's name: ";
    std::cin >> name; 

    // search if contact exists
    bool isCreated = false;
    for (const auto& ct : this->glo) {
        if (ct->getName() == name) {
            isCreated = true;
        }
    }
    if (!isCreated) {
        // ask for last name
        std::cout << "Please enter new contact's last name: ";
        std::cin >> lastName; 

        // ask for phone number
        std::cout << "Please enter new contact's phone number: ";
        std::cin >> number; 

        // create new contact
        std::shared_ptr<Contact> ct(new Contact(name,lastName,number));
        std::cout << "\033[1;32mContact has been created...\033[0m" << std::endl;
        return ct;

    } else {
        std::cout << name << " \033[1;31malready exists...\033[0m";
        std::cout << "\033[1;31mExiting...\033[0m\n";
    }

    return nullptr;
}



void Registery::AddContact(std::shared_ptr<Contact> ct) {

    this->glo.push_back(ct); 
    PROD_MSG("\033[1;32m... and has been added to the registery!\033[0m");

}




bool Registery::SearchContact(cstring name) const {

    bool isCreated = false; // sensor to check the presence of contact

    // looking ..
    for (const auto& ct : this->glo) {
        if (ct->getName() == name) {
            isCreated = true;
            PROD_MSG("\033[1;32mFound it!\033[0m\n");
            PROD_FCT(ct->Display());
        }
    }

    // warning if contact has not been found in registery
    if (!isCreated) {
        PROD_MSG("\033[1;31mContact does not exist.\033[0m\n");
        PROD_MSG("\033[1;31mExiting...\033[0m\n");
    }

    return isCreated;
}




bool Registery::DeleteContact(cstring name) {

    // check if contact exists
    bool isCreated = false;
    std::vector<std::shared_ptr<Contact> >::iterator it;


    for (it = glo.begin() ; it != glo.end() ; ++it) {
        if ((*it)->getName() == name) {
            isCreated = true;
            // delete *it; // no need to delete since smart pointers are used
            glo.erase(it);
            PROD_MSG("\033[1;32mContact has been deleted!\033[0m\n");
            return isCreated;
        }
    }

    // warning if contact has not been found in registery
    if (!isCreated) {
        PROD_MSG("\033[1;31mContact does not exist.\033[0m\n");
        PROD_MSG("\033[1;31mExiting...\033[0m\n");
    }

    return isCreated;
}

cstring Registery::AskUserName() {
    std::string nameContact;
    std::cout << "Enter the name of the contact you are looking for: ";
    std::cin >> nameContact;

    return nameContact;
}