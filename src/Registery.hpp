#ifndef DEF_REG_HPP
#define DEF_REG_HPP

#include "Contact.hpp"

class Contact;

class Registery {

    // attributs
    std::vector<std::shared_ptr<Contact> > glo;

    public:
        static cstring AskUserName();
        std::shared_ptr<Contact> CreateContact() const;
        void AddContact(std::shared_ptr<Contact> ct);
        bool DeleteContact(cstring name);
        bool SearchContact(cstring name) const;

        // getters
        inline const size_t getNbrContact() const { return glo.size(); }
};

#endif // DEF_REG_HPP