#include "Menu.hpp"

uint8_t Menu::WelcomeMenu() const {

    putchar('\n');
    std::cout << "---------------------------------" << std::endl;
    std::cout << "[1] Search for contact" << std::endl;
    std::cout << "[2] Add contact to registery" << std::endl;
    std::cout << "[3] Delete contact from registery" << std::endl;
    std::cout << "---------------------------------" << std::endl;
    putchar('\n');


    uint8_t userChoice;
    std::cout << "\033[1;36mChoose an available option : \033[0m";
    std::cin >> userChoice;

    return userChoice;
}

void Menu::Run() const {

    #ifndef PROD
    char answer;
    std::cout << "\033[1;36mThis build is configure for testing purposes and prompt messages are disabled.\033[0m\n";
    std::cout << "\033[1;36mTo change the build configuration, please check src/Preproc.hpp.\033[0m\n";
    putchar('\n');
    std::cout << "\033[1;36mWould you still like to run the app (y/n)? \033[0m";
    std::cin >> answer;

    if (answer != 'y') { return; } // exit program if user want to change configuration
    #endif 

    while (true) {
        uint8_t userChoice = WelcomeMenu();

        switch (userChoice)
        {
        case '1':
            this->reg->SearchContact(this->reg->AskUserName());
            break;
        
        case '2':
            this->reg->AddContact(this->reg->CreateContact());
            break;

        case '3':
            this->reg->DeleteContact(this->reg->AskUserName());
            break;
        
        default:
            break;
        }
    }
}


// singleton
Menu* Menu::menu_ = nullptr;

Menu* Menu::Init() {
    if (menu_ == nullptr) {
        menu_ = new Menu;
    }

    return menu_;
}


