#ifndef DEF_PREPROC_HPP
#define DEF_PREPROC_HPP

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include <memory>

#define TEST

#ifdef PROD
#define PROD_MSG(str) do { std::cout << str << std::endl; } while( false )
#define PROD_FCT(fct) do { fct; } while (false)
#else
#define PROD_MSG(str) do { } while ( false )
#define PROD_FCT(fct) do { } while (false)
#endif

typedef const std::string cstring;

#endif // DEF_PREPROC_HPP