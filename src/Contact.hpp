#ifndef DEF_CONTACT_HPP
#define DEF_CONTACT_HPP

#include "Preproc.hpp"

class Contact {

    std::string name;
    std::string lastName;
    std::string number;


    public:
        Contact() {}
        Contact(std::string na, std::string la, std::string nbr) {
            this->name = na;
            this->lastName = la;
            this->number = nbr;
        }


        // methods
        void Display() const; 

        // getters
        inline cstring getName()     const { return this->name; }
        inline cstring getLastName() const { return this->lastName; }
        inline cstring getNbr()      const { return this->number; }

        // setters
        inline void setName(std::string na)     { this->name = na; }
        inline void setLastName(std::string la) { this->lastName = la; }
        inline void setNbr(std::string nbr)     { this->number = nbr; } 
};


#endif // DEF_CONTACT_HPP