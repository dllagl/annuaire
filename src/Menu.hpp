#ifndef DEF_MENU_HPP
#define DEF_MENU_HPP

#include "Registery.hpp"

class Registery;
class Contact;

class Menu {

    Registery* reg;
    static Menu* menu_;
    Menu() { this->reg = new Registery; }

    public:
        ~Menu() { delete this->reg; }
        uint8_t WelcomeMenu() const;
        void Run() const;

        // singleton related
        static Menu* Init();
        Menu(Menu& copy) = delete; // prevents copy
        void operator=(const Menu&) = delete; // prevents assignement

};

#endif // DEF_MENU_HPP