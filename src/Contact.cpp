#include "Contact.hpp"



void Contact::Display() const {

    std::cout << "Name:         " << this->name << std::endl;
    std::cout << "Last name:    " << this->lastName << std::endl;
    std::cout << "Phone number: " << this->number << std::endl;
    
}