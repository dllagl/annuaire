#include "TestContact.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(TestContact);

void TestContact::setUp() {
    ct = new Contact("anthony", "dallagnol", "1234");
}

void TestContact::tearDown() {
    delete ct;
}

void TestContact::getName(){
    CPPUNIT_ASSERT(ct->getName() == "anthony");
}

void TestContact::getLastName(){
    CPPUNIT_ASSERT(ct->getLastName() == "dallagnol");
}

void TestContact::getNumber(){
    CPPUNIT_ASSERT(ct->getNbr() == "1234");
}

void TestContact::setName(){
    ct->setName("Aurelien");
    CPPUNIT_ASSERT(ct->getName() == "Aurelien");
}

void TestContact::setLastName(){
    ct->setLastName("lascouts");
    CPPUNIT_ASSERT(ct->getLastName() == "lascouts");
}

void TestContact::setNumber(){
    ct->setNbr("78945");
    CPPUNIT_ASSERT(ct->getNbr() == "78945");
}