#include "TestRegistery.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(TestRegistery);


void TestRegistery::setUp() {
    this->reg = new Registery;
}

void TestRegistery::tearDown() {
    delete this->reg;
}

void TestRegistery::addContact() {
    std::shared_ptr<Contact> ct(
        new Contact("anthony","dallagnol","1234")
        );

    CPPUNIT_ASSERT(this->reg->getNbrContact() == 0);
    this->reg->AddContact(ct);
    CPPUNIT_ASSERT(this->reg->getNbrContact() == 1);

}

void TestRegistery::searchContact() {
    std::shared_ptr<Contact> ct(
        new Contact("anthony","dallagnol","1234")
        );

    this->reg->AddContact(ct);
    CPPUNIT_ASSERT(this->reg->SearchContact("toto") == false);
    CPPUNIT_ASSERT(this->reg->SearchContact("anthony") == true);
}

void TestRegistery::delContact() {
    std::shared_ptr<Contact> ct(
        new Contact("anthony","dallagnol","1234")
        );

    this->reg->AddContact(ct);
    CPPUNIT_ASSERT(this->reg->getNbrContact() == 1);
    CPPUNIT_ASSERT(this->reg->SearchContact("anthony") == true);
    this->reg->DeleteContact("anthony");
    CPPUNIT_ASSERT(this->reg->getNbrContact() == 0);
    CPPUNIT_ASSERT(this->reg->SearchContact("anthony") == false);
}