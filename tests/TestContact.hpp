#ifndef DEF_TEST_CONTACT_HPP
#define DEF_TEST_CONTACT_HPP

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestAssert.h>
#include "../src/Contact.hpp"

class TestContact : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestContact); 
    CPPUNIT_TEST(getName);
    CPPUNIT_TEST(getLastName);
    CPPUNIT_TEST(getNumber);
    CPPUNIT_TEST(setName);
    CPPUNIT_TEST(setLastName);
    CPPUNIT_TEST(setNumber);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp();    
    void tearDown(); 

protected:

    // getters tests
    void getName();
    void getLastName();
    void getNumber();

    // setters tests
    void setName();
    void setLastName();
    void setNumber();


private:
    Contact* ct;
};

#endif // DEF_TEST_CONTACT_HPP