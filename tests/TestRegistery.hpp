#ifndef DEF_TEST_REGISTERY_HPP
#define DEF_TEST_REGISTERY_HPP

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestAssert.h>
#include "../src/Registery.hpp"

class TestRegistery : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestRegistery); 
    CPPUNIT_TEST(addContact);
    CPPUNIT_TEST(delContact);
    CPPUNIT_TEST(searchContact);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp();    
    void tearDown(); 

protected:

    void addContact();
    void delContact();
    void searchContact();


private:
    Registery* reg;
};

#endif // DEF_TEST_REGISTERY_HPP