#include "TestContact.hpp"
#include "TestRegistery.hpp"


int main () {

    // Contacts's unit tests
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry& registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    bool wasSuccessful = runner.run();

    if (wasSuccessful) {
        std::cout << "\033[1;32mProgram's ready for production, please check src/Preproc.hpp to update.\033[0m" << std::endl;
    }


    return wasSuccessful;
}